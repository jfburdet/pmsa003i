defmodule PMSA003I.Comm do
  alias Circuits.I2C

  @spec discover([byte]) :: {binary, byte}
  def discover(possible_addresses \\ [0x12]) do
    I2C.discover_one!(possible_addresses)
  end

  def open(bus_name) do
    {:ok, i2c} = I2C.open(bus_name)
    i2c
  end
end
