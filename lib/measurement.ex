defmodule PMSA003I.Measurement do
  @type t() :: %__MODULE__{
          pm1_std: Integer.t(),
          pm2_5_std: Integer.t(),
          pm10_std: Integer.t(),
          pm1_atmo: Integer.t(),
          pm2_5_atmo: Integer.t(),
          pm10_atmo: Integer.t(),
          part_count0_3: Integer.t(),
          part_count0_5: Integer.t(),
          part_count1: Integer.t(),
          part_count2_5: Integer.t(),
          part_count5: Integer.t(),
          part_count10: Integer.t()
        }

  defstruct [
    :pm1_std,
    :pm2_5_std,
    :pm10_std,
    :pm1_atmo,
    :pm2_5_atmo,
    :pm10_atmo,
    :part_count0_3,
    :part_count0_5,
    :part_count1,
    :part_count2_5,
    :part_count5,
    :part_count10
  ]
end
