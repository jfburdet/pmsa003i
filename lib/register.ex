defmodule PMSA003I.Register do
  alias Circuits.I2C

  @reg_addr 0x02

  @spec read(reference, byte) :: :error | PMSA003I.Measurement.t()
  def read(i2c, device_addr) do
    case I2C.write_read(i2c, device_addr, [@reg_addr], 32) do
      {:ok, data} -> data |> decode
      _ -> :error
    end
  end

  defp decode(<<
         0x42::8,
         0x4D::8,
         _frame_length::16,
         pm1_std::16,
         pm2_5_std::16,
         pm10_std::16,
         pm1_atmo::16,
         pm2_5_atmo::16,
         pm10_atmo::16,
         part_count0_3::16,
         part_count0_5::16,
         part_count1::16,
         part_count2_5::16,
         part_count5::16,
         part_count10::16,
         _rest::bitstring
       >>) do
    %PMSA003I.Measurement{
      pm1_std: pm1_std,
      pm2_5_std: pm2_5_std,
      pm10_std: pm10_std,
      pm1_atmo: pm1_atmo,
      pm2_5_atmo: pm2_5_atmo,
      pm10_atmo: pm10_atmo,
      part_count0_3: part_count0_3,
      part_count0_5: part_count0_5,
      part_count1: part_count1,
      part_count2_5: part_count2_5,
      part_count5: part_count5,
      part_count10: part_count10
    }
  end
end
