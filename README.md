# PMSA003I

[![Hex version](https://img.shields.io/hexpm/v/pmsa003i.svg "Hex version")](https://hex.pm/packages/pmsa003i)

Driver for the [PMSA003I partical concentration sensor](https://www.adafruit.com/product/4632).

[Datasheet](https://cdn-shop.adafruit.com/product-files/4632/4505_PMSA003I_series_data_manual_English_V2.6.pdf)


## Installation

```elixir
def deps do
  [
    {:pmsa003i, "~> 0.1"}
  ]
end
```

## Sample usage

### Using sensor commands

```elixir
ex(1)> {bus_name, device_addr} = PMSA003I.Comm.discover
{"i2c-1", 18}
iex(2)> i2c = PMSA003I.Comm.open(bus_name)
#Reference<0.3889352237.268828678.103225>
iex(3)> PMSA003I.Register.read(i2c, device_addr)
%PMSA003I.Measurement{
  part_count0_3: 3624,
  part_count0_5: 1119,
  part_count1: 196,
  part_count10: 0,
  part_count2_5: 4,
  part_count5: 2,
  pm10_atmo: 38,
  pm10_std: 38,
  pm1_atmo: 23,
  pm1_std: 26,
  pm2_5_atmo: 33,
  pm2_5_std: 36
}
```

### Using GenServer

```elixir

iex(1)> PMSA003I.start_link([name: :pmsa003i, polling_interval: 10_000])
{:ok, #PID<0.1358.0>}
iex(2)> :timer.sleep(10_000)
:ok
iex(3)> GenServer.call(:pmsa003i, :measure)
{:ok,
 %PMSA003I.Measurement{
   part_count0_3: 4164,
   part_count0_5: 1263,
   part_count1: 211,
   part_count10: 2,
   part_count2_5: 13,
   part_count5: 2,
   pm10_atmo: 43,
   pm10_std: 43,
   pm1_atmo: 25,
   pm1_std: 29,
   pm2_5_atmo: 36,
   pm2_5_std: 41
 }}
```
